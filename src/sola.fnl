(local sola {})
(local lume (require "lume"))
(local lpeg (require "lpeg"))
(local inspect (require "inspect"))
(local opse (require "frameworks/OPSE"))

;;=====[ PEG Grammar ]=====
(fn denote [tbl]
  "Transform PEG table into queryable result"
  (local denotation {"die" nil
                     "num" 1
                     "mod" nil
                     "mamt" nil})
  (match (length tbl)
    1 (do
        (tset denotation "die" (. tbl 1)))
    2 (do
        (tset denotation "num" (. tbl 1))
        (tset denotation "die" (. tbl 2)))
    3 (do
        (tset denotation "die" (. tbl 1))
        (tset denotation "mod" (. tbl 2))
        (tset denotation "mamt" (. tbl 3)))
    4 (do
        (tset denotation "num" (. tbl 1))
        (tset denotation "die" (. tbl 2))
        (tset denotation "mod" (. tbl 3))
        (tset denotation "mamt" (. tbl 4))))
  denotation)
;;Use: (denote (parse:match "2d6+1"))

;;Define an integer
(local integer (/ (^ (lpeg.R :09) 1) tonumber))
;;Define the rune d
(local delta (^ (lpeg.S :d) 1))
;;Define die modifications
(local modification (lpeg.C (lpeg.S "+-/*")))
;;Grammar definition, matches any of the following formats:
;;d6, d6+1, 2d6, 2d6+1, 10d6/2, 100d6*2 (obviously we're playing shadowrun)
;;Lucidiot would like to be able to parse 4d(3d2)^d5
(local parse
       (lpeg.P {1 :input
                :input (* (lpeg.V :exp) (- 1))
                :die (* delta integer)
                :xdiemod (+ (lpeg.V :xdie) (* modification integer))
                :exp (lpeg.Ct (* (^ (+ (lpeg.V :xdie) (lpeg.V :xdiemod)) 0)
                        (^ (+ (lpeg.V :die) (lpeg.V :diemod)) 0)))
                :xdie (* (* integer delta) integer)
                :diemod (+ (lpeg.V :die) (* modification integer))}))
;;Use: (parse:match "2d6+10")

;;=====[ Utilities ]=====
(fn util/split [val check]
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)
;;Use: (split "2d12" "d")

;;=====[ Dice ]=====
(fn sola.roll [die]
  "Roll against a die syntax. IE: (roll \"2d12+1\") -> roll d12 2x modify result +1"
  (let
      [dtbl (denote (parse:match die))
       die (. dtbl "die")
       num (. dtbl "num")
       mod (. dtbl "mod")
       mamt (. dtbl "mamt")
       rolls {}]
    (var result 0)
    (math.randomseed (os.time))
    (for [i 1 num]
      (table.insert rolls (math.random die)))
    (each [_ v (pairs rolls)]
      (set result (+ result v)))
    (match mod
      "+" (set result (+ result mamt))
      "-" (set result (- result mamt))
      "/" (set result (/ result mamt))
      "*" (set result (* result mamt)))
    (math.floor result)))
;;Use: (roll "2d12+1")

(fn sola.weighted_roll [action die]
  "Rolls a die, depending on the min, max values in the framework rolled against, returns a result"
  (var ret "")
  (let
      [die_roll (sola.roll die)]
    (each [k v (pairs action)]
      (let
          [min (. action k "Min")
           max (. action k "Max")]
        (if
         (and (<= min die_roll) (>= max die_roll))
         (set ret (. action k "Result"))))))
  ret)
;;Use: (weighted_roll framework.Tables.Oracle.Yes_No "1d6")
;;Used on tables where a range of between x & y is used to determine a result

(fn sola.raw_roll [action die]
  "Rolls a die, returns a result based on index"
  (var ret "")
  (let
      [die_roll (sola.roll die)]
    (set ret (. action die_roll)))
  ret)
;;Use: (raw_roll framework.Tables.Oracle.Domain "1d4")
;;Used on tables that have a direct 1 to 1 correlation

;;=====[ Frameworks ]=====
(fn sola.handle [action]
  (let
      [ret {}]
    (each [k v (ipairs action)]
      (do
        (print k v)
        (if (= (type (. action k)) "table")
            (do
              (table.insert ret
                            (sola.weighted_roll (. action k 1) (. action k 2))))
            (do
              (table.insert ret (. action k))))))
    (print (inspect ret))
    (each [k v (pairs (. action "Map"))]
      (do
        (var last "")
        (var collection [])
        (if (= (. action "Map" k) nil)
            (do
              (print (. ret k))
              (set last nil)
              (table.insert collection (. ret k)))
            (~= (. action "Map" k) nil)
            (do
              (if (= last nil)
                  (do
                    (var str "")
                    (each [k v (pairs collection)]
                      (.. str v))
                    (print str)))
              (print (.. (. action "Map" k) (. ret k)))
              (set last "table")
              (set collection [])))))))
;;Use: (sola.handle opse.Engine.GameMaster.NPC) <- generate an NPC based on the OPSE engine
;;Used to action engine callbacks

sola
