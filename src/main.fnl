#!/usr/bin/fennel
(local sola (require "sola"))
(local opse (require "frameworks/OPSE"))
(local tests (require "tests"))

(fn main [?argv1 ?argv2]
  (if (= ?argv1 "--test")
      (tests.do_opse)))

(main ...)
