local opse = {}

opse.info = {
   license = "CC-BY-SA 4.0",
   author = "Karl Hendricks",
   studio = "Inflatable Studios",
   source = "https://inflatablestudios.itch.io/one-page-solo-engine",
}

--This is a one to one mapping of the tables in OPSE which define Results, and Min/Max weights for rolls 
opse.Tables = {
   Scene = {
	  Complication = {
		 {Result = "Hostile forces oppose you.", Min = 1, Max = 1},
		 {Result = "An obstacle blocks your way.", Min = 2, Max = 2},
		 {Result = "Wouldn't it suck if...", Min = 3, Max = 3},
		 {Result = "An NPC acts suddenly.", Min = 4, Max = 4},
		 {Result = "All is not as it seems.", Min = 5, Max = 5},
		 {Result = "Things actually go as planned.", Min = 6, Max = 6},
	  },
	  Alteration = {
		 {Result = "A major detail of the scene is enhanced or somehow worse.", Min = 1, Max = 1},
		 {Result = "The environment is different.", Min = 2, Max = 2},
		 {Result = "Unexpected NPCs are present.", Min = 3, Max = 3},
		 {Result = "Add a complication.", Min = 4, Max = 4},
		 {Result = "Add a pacing move.", Min = 5, Max = 5},
		 {Result = "Add a random event.", Min = 6, Max = 6},
	  }
   },
   Oracle = {
	  Yes_No = {
		 Likely = {Result = "Yes", Min = 3, Max = 6},
		 Even = {Result = "Yes", Min = 4, Max = 6},
		 Unlikely = {Result = "Yes", Min = 5, Max = 6},
	  },
	  Modifier = {
		 {Result = "but...", Min = 1, Max = 1},
		 {Result = "", Min = 2, Max = 5},
		 {Result = "and...", Min = 6, Max = 6},
	  },
	  How = {
		 {Result = "Suprisingly lacking", Min = 1, Max = 1},
		 {Result = "Less than expected", Min = 2, Max = 2},
		 {Result = "About average", Min = 3, Max = 4},
		 {Result = "More than expected", Min = 5, Max = 5},
		 {Result = "Extraordinary", Min = 6, Max = 6},
	  },
	  Domain = {
		 {Result = "Physical (appearance, existence)", Min = 1, Max = 1},
		 {Result = "Technical (mental, operation)", Min = 2, Max = 2},
		 {Result = "Mystical (meaing, capability)", Min = 3, Max = 3},
		 {Result = "Social (personal, connection)", Min = 4, Max = 4},
	  },
	  Action = {
		 {Result = "Seek", Min = 1, Max = 1},
		 {Result = "Oppose", Min = 2, Max = 2},
		 {Result = "Communicate", Min = 3, Max = 3},
		 {Result = "Move", Min = 4, Max = 4},
		 {Result = "Harm", Min = 5, Max = 5},
		 {Result = "Create", Min = 6, Max = 6},
		 {Result = "Reveal", Min = 7, Max = 7},
		 {Result = "Command", Min = 8, Max = 8},
		 {Result = "Take", Min = 9, Max = 9},
		 {Result = "Protect", Min = 10, Max = 10},
		 {Result = "Assist", Min = 11, Max = 11},
		 {Result = "Transform", Min = 12, Max = 12},
		 {Result = "Decieve", Min = 13, Max = 13},
	  },
	  Detail = {
		 {Result = "Small", Min = 1, Max = 1},
		 {Result = "Large", Min = 2, Max = 2},
		 {Result = "Old", Min = 3, Max = 3},
		 {Result = "New", Min = 4, Max = 4},
		 {Result = "Mundane", Min = 5, Max = 5},
		 {Result = "Simple", Min = 6, Max = 6},
		 {Result = "Complex", Min = 7, Max = 7},
		 {Result = "Unsavory", Min = 8, Max = 8},
		 {Result = "Specialized", Min = 9, Max = 9},
		 {Result = "Unexpected", Min = 10, Max = 10},
		 {Result = "Exotic", Min = 11, Max = 11},
		 {Result = "Dignified", Min = 12, Max = 12},
		 {Result = "Unique", Min = 13, Max = 13},
	  },
	  Topic = {
		 {Result = "Current Need", Min = 1, Max = 1},
		 {Result = "Allies", Min = 2, Max = 2},
		 {Result = "Community", Min = 3, Max = 3},
		 {Result = "History", Min = 4, Max = 4},
		 {Result = "Future Plans", Min = 5, Max = 5},
		 {Result = "Enemies", Min = 6, Max = 6},
		 {Result = "Knowledge", Min = 7, Max = 7},
		 {Result = "Rumors", Min = 8, Max = 8},
		 {Result = "A Plot Arc", Min = 9, Max = 9},
		 {Result = "Recent Events", Min = 10, Max = 10},
		 {Result = "Equipment", Min = 11, Max = 11},
		 {Result = "A Faction", Min = 12, Max = 12},
		 {Result = "The PCs", Min = 13, Max = 13},
	  },
   },
   GameMaster = {
	  Pacing = {
		 {Result = "Foreshadow Trouble", Min = 1, Max = 1},
		 {Result = "Reveal a New Detail", Min = 2, Max = 2},
		 {Result = "An NPC Takes Action", Min = 3, Max = 3},
		 {Result = "Advance a Threat", Min = 4, Max = 4},
		 {Result = "Advance a Plot", Min = 5, Max = 5},
		 {Result = "Add a random event to the scene", Min = 6, Max = 6},
	  },
	  Failure = {
		 {Result = "Cause Harm", Min = 1, Max = 1},
		 {Result = "Put Someone in a Spot", Min = 2, Max = 2},
		 {Result = "Offer a Choice", Min = 3, Max = 3},
		 {Result = "Advance a Threat", Min = 4, Max = 4},
		 {Result = "Reveal an Unwelcome Truth", Min = 5, Max = 5},
		 {Result = "Foreshadow Trouble", Min = 6, Max = 6},
	  },
	  Objective = {
		 {Result = "Eliminate a threat", Min = 1, Max = 1},
		 {Result = "Learn the truth", Min = 2, Max = 2},
		 {Result = "Recover something valuable", Min = 3, Max = 3},
		 {Result = "Escort or deliver to safety", Min = 4, Max = 4},
		 {Result = "Restore something broken", Min = 5, Max = 5},
		 {Result = "Save an alley in peril", Min = 6, Max = 6},
	  },
	  Adversaries = {
		 {Result = "A powerful organization", Min = 1, Max = 1},
		 {Result = "Outlaws", Min = 2, Max = 2},
		 {Result = "Guardians", Min = 3, Max = 3},
		 {Result = "Local inhabitants", Min = 4, Max = 4},
		 {Result = "Enemy horde or force", Min = 5, Max = 5},
		 {Result = "A new or recurring villain", Min = 6, Max = 6},
	  },
	  Rewards = {
		 {Result = "Money or valuables", Min = 1, Max = 2},
		 {Result = "Knowledge and secrets", Min = 3, Max = 3},
		 {Result = "Support of an ally", Min = 4, Max = 4},
		 {Result = "Advance a plot arc", Min = 5, Max = 5},
		 {Result = "An item of power", Min = 6, Max = 6},
	  },
	  NPC = {
		 Identity = {
			{Result = "Outlaw", Min = 1, Max = 1},
			{Result = "Drifter", Min = 2, Max = 2},
			{Result = "Tradesman", Min = 3, Max = 3},
			{Result = "Commoner", Min = 4, Max = 4},
			{Result = "Soldier", Min = 5, Max = 5},
			{Result = "Merchant", Min = 6, Max = 6},
			{Result = "Specialist", Min = 7, Max = 7},
			{Result = "Entertainer", Min = 8, Max = 8},
			{Result = "Adherent", Min = 9, Max = 9},
			{Result = "Leader", Min = 10, Max = 10},
			{Result = "Mystic", Min = 11, Max = 11},
			{Result = "Adventurer", Min = 12, Max = 12},
			{Result = "Lord", Min = 13, Max = 13},
		 },
		 Goal = {
			{Result = "Obtain", Min = 1, Max = 1},
			{Result = "Learn", Min = 2, Max = 2},
			{Result = "Harm", Min = 3, Max = 3},
			{Result = "Restore", Min = 4, Max = 4},
			{Result = "Find", Min = 5, Max = 5},
			{Result = "Travel", Min = 6, Max = 6},
			{Result = "Protect", Min = 7, Max = 7},
			{Result = "Enrich Self", Min = 8, Max = 8},
			{Result = "Avenge", Min = 9, Max = 9},
			{Result = "Fulfill Duty", Min = 10, Max = 10},
			{Result = "Escape", Min = 11, Max = 11},
			{Result = "Create", Min = 12, Max = 12},
			{Result = "Serve", Min = 13, Max = 13},
		 },
		 Features = {
			{Result = "Unremarkable", Min = 1, Max = 1},
			{Result = "Notable nature", Min = 2, Max = 2},
			{Result = "Obvious physical trait", Min = 3, Max = 3},
			{Result = "Quirk or mannerism", Min = 4, Max = 4},
			{Result = "Unusual equipment", Min = 5, Max = 5},
			{Result = "Unexpected age or origin", Min = 6, Max = 6},
		 },
	  },
   },
   Dungeon = {
	  Initial_Features = {
		 "Three exits, the one you entered by and 2 additional exits."
	  },
	  Location = {
		 {Result = "Typical area", Min = 1, Max = 1},
		 {Result = "Transitional area", Min = 2, Max = 2},
		 {Result = "Living area or meeting place", Min = 3, Max = 3},
		 {Result = "Working or utility area", Min = 4, Max = 4},
		 {Result = "Area with special feature", Min = 5, Max = 5},
		 {Result = "Location with specialized purpose", Min = 6, Max = 6},
	  },
	  Encounter = {
		 {Result = "None", Min = 1, Max = 2},
		 {Result = "Hostile enemies", Min = 3, Max = 4},
		 {Result = "An obstacle blocks the way", Min = 5, Max = 5},
		 {Result = "Unique NPC or adversary", Min = 6, Max = 6},
	  },
	  Object = {
		 {Result = "Nothing, or mundane objects", Min = 1, Max = 2},
		 {Result = "Am interesting item or clue", Min = 3, Max = 3},
		 {Result = "A useful tool, key, or device", Min = 4, Max = 4},
		 {Result = "Something valuable", Min = 5, Max = 5},
		 {Result = "Rare or special item", Min = 6, Max = 6},
	  },
	  Exits = {
		 {Result = "Dead end", Min = 1, Max = 2},
		 {Result = "1 additional exits", Min = 3, Max = 4},
		 {Result = "2 additional exits", Min = 5, Max = 6},
	  },
   },
   HewCrawler = {
	  Terrain = {
		 {Result = "Same as current hex", Min = 3, Max = 3},
		 {Result = "Common terrain", Min = 4, Max = 4},
		 {Result = "Uncommon terrain", Min = 5, Max = 5},
		 {Result = "Rare terrain", Min = 6, Max = 6},
	  },
	  Contents = {
		 {Result = "Nothing notable", Min = 3, Max = 3},
		 {Result = "Roll a feature", Min = 4, Max = 4},
	  },
	  Features = {
		 {Result = "Notable structure", Min = 1, Max = 1},
		 {Result = "Dangerous hazard", Min = 2, Max = 2},
		 {Result = "A settlement", Min = 3, Max = 3},
		 {Result = "Strange natural feature", Min = 4, Max = 4},
		 {Result = "New region (start new terrain types)", Min = 5, Max = 5},
		 {Result = "Dungeon Entrance", Min = 6, Max = 6},
	  },
	  Event = {
		 {Result = "None", Min = 1, Max = 4},
		 {Result = "Random event, then set the scene", Min = 5, Max = 6},
	  },
   }
}

--This is a mapping of the various steps to produce an OPSE engine action defined as a table, and the call needed on the table
--Each step in the engine is made up of a table containing one or more steps
opse.Engine = {
   GameMaster = {
	  Random_Event = {
		 {opse.Tables.Oracle.Action, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 {opse.Tables.Oracle.Topic, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 Map = {"Action: ", "Action Domain: ", "Topic: ", "Topic Domain: "},
	  },
	  Situation = {
		 {opse.Tables.Oracle.How, "1d6"},
		 {opse.Tables.Oracle.Topic, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 Map = {"How: ", "Topic: ", "Topic Domain: "},
	  },
	  NPC = {
		 {opse.Tables.GameMaster.NPC.Identity, "1d13"},
		 {opse.Tables.GameMaster.NPC.Goal, "1d13"},
		 {opse.Tables.GameMaster.NPC.Features, "1d6"},
		 {opse.Tables.Oracle.Detail, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 {opse.Tables.Oracle.How, "1d6"},
		 {opse.Tables.Oracle.Topic, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 Map = {"Identity: ", "Goal: ", "Features: ", "Detail: ", "Detail Domain: ", "How: ", "Topic: ", "Topic Domain: "},
	  },
	  Plot_Hook = {
		 {opse.Tables.GameMaster.Objective, "1d6"},
		 {opse.Tables.GameMaster.Adversaries, "1d6"},
		 {opse.Tables.GameMaster.Rewards, "1d6"},
		 Map = {"Objective: ", "Adversaries: ", "Rewards: "},
	  },
	  Generic = {
		 {opse.Tables.Oracle.Action, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 {opse.Tables.Oracle.Detail, "1d13"},
		 {opse.Tables.Oracle.Domain, "1d4"},
		 {opse.Tables.Oracle.How, "1d6"},
		 Map = {"Action: ", "Action Domain: ", "Detail: ", "Detail Domain: ", "How: "},
	  },
	  
   },
   Dungeon = {
	  Theme = {
		 opse.Tables.Oracle.Detail,
		 opse.Tables.Oracle.Focus
	  },
   },
}

return opse
