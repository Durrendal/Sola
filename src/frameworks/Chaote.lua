local chaote = {}

chaote.info = {
   license = "",
   author = "dozens",
   studio = "dozens",
   source = "https://dozens.itch.io/chaote!",
}

chaote.Tables = {
   Scene = {
	  Adventure_time = {
		 Foreword = "There's a crisis!",
		 Adversaries = {
			{Result = "The Bermuda Triangle is ", Min = 1, Max = 1},
			{Result = "The Bavarian Illuminati are ", Min = 2, Max = 2},
			{Result = "Lizard People are ", Min = 3, Max = 3},
			{Result = "UFO cult is ", Min = 4, Max = 4},
			{Result = "Erisian Liberation Front is ", Min = 5, Max = 5},
			{Result = "Children of Cthulhu are ", Min = 6, Max = 6},
		 },
		 Complication = {
			{Result = "buying up ", Min = 1, Max = 1},
			{Result = "brainwashing ", Min = 2, Max = 2},
			{Result = "poisoning ", Min = 3, Max = 3},
			{Result = "stealing ", Min = 4, Max = 4},
			{Result = "ransoming ", Min = 5, Max = 5},
			{Result = "robbing ", Min = 6, Max = 6},
		 },
		 Crisis = {
			{Result = "the consitution!", Min = 1, Max = 1},
			{Result = "children!", Min = 2, Max = 2},
			{Result = "our traditions!", Min = 3, Max = 3},
			{Result = "reality!", Min = 4, Max = 4},
			{Result = "world leaders!", Min = 5, Max = 5},
			{Result = "our minds!", Min = 6, Max = 6},
		 },
	  },
	  Infiltration = {
		 Foreword = "You have infiltrated their ",
		 Lair = {
			{Result = "volcano lair, ", Min = 1, Max = 1},
			{Result = "moon base, ", Min = 2, Max = 2},
			{Result = "pizzeria, ", Min = 3, Max = 3},
			{Result = "ocean floor lab, ", Min = 4, Max = 4},
			{Result = "airship, ", Min = 5, Max = 5},
			{Result = "island fortress, ", Min = 6, Max = 6},
		 },
		 Complication = {
			{Result = "but the princess is in another castle.", Min = 1, Max = 1},
			{Result = "but, you're being monitored by cameras & CCTV wherever you go.", Min = 2, Max = 2},
			{Result = "but, the ritual has already begun!", Min = 3, Max = 3},
			{Result = "but, there are armed guards everywhere!", Min = 4, Max = 4},
			{Result = "but, it's haunted by quantum ghosts!", Min = 5, Max = 5},
			{Result = "but, you've been detained by Hollywood elites!", Min = 6, Max = 6},
		 },
	  },
   },
   Oracle = {
	  --And is an intensifier, it makes a good thing better and a bad thing worse
	  --But is a softener, it makes a bad thing more palatable and a good thing complicated
	  Yes_No = {
		 {Result = "No, and..", Min = 1, Max = 1},
		 {Result = "No", Min = 2, Max = 2},
		 {Result = "No, but..", Min = 3, Max = 3},
		 {Result = "Yes, but..", Min = 4, Max = 4},
		 {Result = "Yes", Min = 5, Max = 5},
		 {Result = "Yes, and..", Min = 6, Max = 6},
	  },
	  Disposition = {
		 {Result = "Hostile", Min = 1, Max = 1},
		 {Result = "Cautious", Min = 2, Max = 3},
		 {Result = "Friendly", Min = 4, Max = 5},
		 {Result = "Helpful", Min = 6, Max = 6},
	  },
   },
   GameMaster = {
	  --Weak elements provide a - 1d6 debuff from total roll
	  --Strong elements provide a + 1d6 buff to total roll
	  --Neutral elements operate @ chaos/order level
	  Elements = {
		 Sweet = {Atributes = "joy, support, sweetness, positivity, taste",
				  Strong = {"Boom", "Prickle"},
				  Weak = {"Orange", "Pungent"},
				  Neutral = "Sweet"},
		 Boom = {Attributes = "sudden, unexpected, powerful, vigorous but brief, sound",
				 Strong = {"Orange", "Pungent"},
				 Weak = {"Sweet", "Prickle"},
				 Neutral = "Boom"},
		 Pungent = {Attributes = "slow build up, suspense, creeping, stealth, trickery, sensuality, smell",
					Strong = {"Prickle", "Sweet"},
					Weak = {"Orange", "Boom"},
					Neutral = "Pungent"},
		 Prickle = {Attributes = "alertness, wakefulness, brash, intensity, sensation, touch",
					Strong = {"Orange", "Boom"},
					Weak = {"Sweet", "Pungent"},
					Neutral = "Prickle"},
		 Orange = {Attributes = "confusion, dissonance, uncertainty, nourishment, sight",
				   Strong = {"Sweet", "Pungent"},
				   Weak = {"Prickle", "Boom"},
				   Neutral = "Orange"},
	  },
	  NPC = {
		 Description = {
			{Result = "Rookie", Min = 1, Max = 1},
			{Result = "Paranoid", Min = 2, Max = 2},
			{Result = "Cynic", Min = 3, Max = 3},
			{Result = "Washed up", Min = 4, Max = 4},
			{Result = "Retired", Min = 5, Max = 5},
			{Result = "Incompetent", Min = 6, Max = 6},
		 },
		 Role = {
			{Result = "Face Puncher", Min = 1, Max = 1},
			{Result = "Galaxy Brain", Min = 2, Max = 2},
			{Result = "Hackerman", Min = 3, Max = 3},
			{Result = "Sticyfingers", Min = 4, Max = 4},
			{Result = "Smoothtalker", Min = 5, Max = 5},
			{Result = "Occultist", Min = 6, Max = 6},
		 },
		 Alignment = {
			{Result = "Boom", Min = 1, Max = 1},
			{Result = "Prickle", Min = 2, Max = 2},
			{Result = "Sweet", Min = 3, Max = 3},
			{Result = "Pungent", Min = 4, Max = 4},
			{Result = "Orange", Min = 5, Max = 5},
			{Result = "Roll Again", Min = 6, Max = 6},
		 },
		 Stats = {
			Chaos = 3,
			Order = 3,
		 },
	  },
   },
}


chaote.Engine = {
   GameMaster = {
	  NPC = {
		 {chaote.Tables.GameMaster.NPC.Description, "1d6"},
		 {chaote.Tables.GameMaster.NPC.Role, "1d6"},
		 {chaote.Tables.GameMaster.NPC.Alignment, "1d6"},
		 Map = {"Description: ", "Role: ", "Alignment: "},
	  },
	  Character_Sheet = {
		 {chaote.Tables.GameMaster.NPC.Description, "1d6"},
		 {chaote.Tables.GameMaster.NPC.Role, "1d6"},
		 {chaote.Tables.GameMaster.NPC.Alignment, "1d6"},
		 Map = {"Description: ", "Role: ", "Alignment: ", "Stats: "},
	  },
	  --(filter (sola.handle chaote.Engine.GameMaster.Eelemnts) "Sweet")
	  Elements = {
		 chaote.Tables.GameMaster.Elements
	  },
	  Scene = {
		 --change sola.handle to pull value if ~= table
		 chaote.Tables.Scene.Adventure_time.Foreword,
		 {chaote.Tables.Scene.Adventure_time.Adversaries, "1d6"},
		 {chaote.Tables.Scene.Adventure_time.Complication, "1d6"},
		 {chaote.Tables.Scene.Adventure_time.Crisis, "1d6"},
		 chaote.Tables.Scene.Infiltration.Foreword,
		 {chaote.Tables.Scene.Infiltration.Lair, "1d6"},
		 {chaote.Tables.Scene.Infiltration.Complication, "1d6"},
		 Map = {"Scene: ", nil, nil, nil, "Complication: ", nil, nil},
	  },
   },
}

return chaote
