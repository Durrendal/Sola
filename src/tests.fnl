(local tests {})
(local inspect (require "inspect"))
(local sola (require "sola"))
(local opse (require "frameworks/OPSE"))
(local chaote (require "frameworks/Chaote"))

;;=====[ Tests ]=====
(fn tests.do_old []
  (print
   (.. "Rolling:\n"
       "2d6+1: " (sola.roll "2d6+1") "\n"
       "2d6: " (sola.roll "2d6") "\n"
       "d6+1: " (sola.roll "d6+1") "\n"
       "d6: " (sola.roll "d6") "\n"
       "2d6*3: " (sola.roll "2d6*3") "\n"
       "d6-1: " (sola.roll "d6-1") "\n"
       "d6/2: " (sola.roll "d6/2") "\n"))
  (print (inspect opse.Tables))
  (print (inspect opse.MultiCalls))
  (print (.. "Scene: " (sola.weighted_roll opse.Tables.Scene.Complication "1d6")))
  (print (.. "Identity: " (sola.weighted_roll opse.Tables.GameMaster.NPC.Identity "1d13")))
  (print (.. "Goal: " (sola.weighted_roll opse.Tables.GameMaster.NPC.Goal "1d13")))
  (print (.. "Notable Feature: " (sola.weighted_roll opse.Tables.GameMaster.NPC.Features "1d6")))
  (print (.. "Detail: " (sola.weighted_roll opse.Tables.Oracle.Detail "1d12") " " (sola.weighted_roll opse.Tables.Oracle.Domain "1d4")))
  (print "===[handler]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.NPC))))

(fn tests.do_opse []
  (print "===[ NPC ]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.NPC)))
  (print "===[ Random Event ]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.Random_Event)))
  (print "===[ Situation ]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.Situation)))
  (print "===[ Plot Hook ]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.Plot_Hook)))
  (print "===[ Generic ]===")
  (print (inspect (sola.handle opse.Engine.GameMaster.Generic))))

(fn tests.do_chaote []
  (print "===[ NPC ]===")
  (print (inspect (sola.handle chaote.Engine.GameMaster.NPC)))
  (print "===[ Plot ]===")
  (print (inspect (sola.handle chaote.Engine.GameMaster.Scene)))
  (print "===[ Character ]===")
  (print (inspect (sola.handle chaote.Engine.GameMaster.Character_Sheet)))
  (print "===[ Eelements ]===")
  (print (inspect chaote.Engine.GameMaster.Eelements)))

tests

