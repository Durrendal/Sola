#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	$(LUA_ROCKS) install lume --local
	$(LUA_ROCKS) install lpeg --local
	$(LUA_ROCKS) install inspect --local

compile-lua:
	$(MAKE) fetch-libraries
	fennel --compile --require-as-include src/main.fnl > src/sola.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/sola.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/sola.lua

install-lua:
	install ./src/sola.lua -D $(DESTDIR)/sola

compile-bin:
	cd ./src/ && fennel --compile-binary main.fnl sola-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/sola-bin -D $(DESTDIR)/sola
